# Navigate BASH with speed and ease

## Purpose

These scripts are designed to help you navigate BASH faster.  The `mcdir` is
essentially the same as `mkdir` with the exception that it will take you into
the new directory, once it is made.  The `cdd` script provides the same effect
as `cd` except that it prints the new directory's path, then its contents for
the user.

## Download

    cd ~/
    git clone https://bitbucket.org/mdill/bash_directory_navigation.git

## Placement

It is suggested that these scripts be placed in `/usr/local/bin/` for simplicity
and ease-of-use.  Once this is done, `mcdir` and `cdd` can be made executable by
sourcing the files in your `bashrc` with:

    source /usr/local/bin/cdd
    source /usr/local/bin/mcdir

The `filesizes` script can be made executable via:

    chmod +x /usr/local/bin/filesizes

## License

This project is licensed under the BSD License - see the [LICENSE.md](https://bitbucket.org/mdill/bash_directory_navigation/src/ea4a273b136211640d21e3b4c6e06ce16b8edfdc/LICENSE.txt?at=master) file for
details.

